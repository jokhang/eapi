
function success(data, message) {
    return {
        code: 200,
        message: message ? message : '',
        data
    }
}

function error(message) {
    return {
        code: 500,
        message,
        data: null
    }
}

export default { success, error }