# 极简mongodb api快速接口服务

## 开发环境

1. node 18.12.1

### 快速接口http method使用说明

POST（增） DELETE（删） PUT（改） GET（查）

#### 日期数据处理方式：所有日期以数字方式进行存储

常用js代码

```js
// 日期数据转数字
let d = new Date();
let createAt = d.valueOf();

// 数字转日期类型
let d= new Date(1566468841156);

// 前端moment格式化显示
moment(item.createAt).format("YYYY年MM月DD日");
```

---

### 常用代码

创建fatch.js如下：

```js
import axios from 'axios';

const timeout = 90000;
// 创建axios实例
const api = axios.create({
    baseURL:'http://127.0.0.1:7700/api',
    timeout: timeout
});

export { api }
```

---

### 添加单条数据到user表

```js
static async add(item) {
    let ret = await api.post("/user/insert", item);
    if (ret.status == 200) {
      return true;
    } else {
      return false;
    }
  }
```

---

### 添加多条数据到user表

```js
static async addmany(items) {
    let ret = await api.post("/user/insertmany", items);
    if (ret.status == 200) {
      return true;
    } else {
      return false;
    }
  }
```

---

### 修改user表单条数据

```js
static async edit(item) {
    let r = await api.put("/user/update", item);
    if (r.status == 200) {
      return true;
    } else {
      return false;
    }
  }
```

---

### 根据用户id取单条数据

```js
static async getById(item) {
    let r = await api.get("/user/638ac822ceff3fd3a123049a");
    if (r.status == 200) {
      return true;
    } else {
      return false;
    }
  }
```

---

### 根据用户id删除单条数据

```js
static async delById(item) {
    let r = await api.delete("/user/638ac822ceff3fd3a123049a");
    if (r.status == 200) {
      return true;
    } else {
      return false;
    }
  }
```

---

### 分页查询姓名包含李按创建时间倒序的用户数据

```js
  static async findUsersPage() {
    let params = {
      query: {
        name: {
          $regex: '李',
        } 
       },
      order: [['createTime', -1]],
      pagesize: 10,
      pageindex: 1,
    }
    let r = await api.post("/user/findpage", params);
    if (r.status == 200) {
      // 数据
      // r.data.dataList
      // 总数量
      // r.data.count
      return r.data;
    } else {
      return false;
    }
  }
```

---

### 查询符合条件的一条数据

```js
  static async findUserOne() {
    let params = {
      query: {
        no: '123',
       }     
    }
    let r = await api.post("/user/findpage", params);
    if (r.status == 200) {
      // 数据
      // r.data.dataList
      // 总数量
      // r.data.count
      return r.data;
    } else {
      return false;
    }
  }
```

---

### 按查询参数查询用户表中名称包含李的数据总量

```js
  static async findUsersCount() {
    let params = {
      query: {
        name: {
          $regex: '李',
        }
       }
    }
    let r = await api.post("/user/findcount", params);
    if (r.status == 200) {     
      return r.data;
    } else {
      return false;
    }
  }
```

### 构建容器

```sh
git clone https://gitee.com/jokhang/eapi.git
cd eapi
docker build -t eapi .
docker save -o eapi.tar eapi
```

### 运行容器

```sh
docker run --name myapi --restart always -p 7000:7000 -e database=xxx -e mongodbUrl=mongodb://user:password@0.0.0.0:27017 -d eapi
```
