import os from 'os';

export default function (fastify, opts, done) {
    fastify.get('/live', async () => {
        return {
            freemem: parseInt(os.freemem() / 1024 / 1024),
            totalmem: parseInt(os.totalmem() / 1024 / 1024),
            loadavg: os.loadavg(),
            platform: os.platform(),
            release: os.release(),
            uptime: os.uptime()
        }
    });

    fastify.get('/', async () => {
        return 1
    });

    done();
}