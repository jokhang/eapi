FROM node:18-alpine
ENV NODE_ENV production
RUN apk add --no-cache tzdata
ENV TZ Asia/Shanghai
WORKDIR /usr/src/app
COPY . .
RUN npm --registry=https://registry.npmmirror.com i --production --silent
EXPOSE 7000
CMD ["node", "index.mjs"]
